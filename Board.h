#ifndef _BOARD_H_INC
#define _BOARD_H_INC

#include <cstdint>
#include <array>
#include <bitset>
#include <memory>
#include "Datatypes.h"

// Location-based board state
// Array of array of pieces
// IMPORTANT: Array is zero-indexed, chess boards are normally one-indexed!
// In here, we use zero-indexation
typedef std::array< std::array< std::shared_ptr<RichPiece>, 8>, 8> SimpleBoard;

// Define bitboards as a fast 64-bit int
// unsigned long long is also fine, but idk which is better
typedef uint_fast64_t bitboard;

// Use 'enum Piece' from Datatypes.h to access the bitboards.
typedef std::array<bitboard, 15> BitboardState;

// Final board representation: a list of pieces.
// There are at most 32 pieces on the board at any moment.
// Use a pointer, so the SimpleBoard can contain references to the PieceBoard.
typedef std::array< std::shared_ptr<RichPiece>, 32> PieceBoard;

class Board {
	public:
		Board();        // Create a standard board
		Board(const Board& toCopy) {
			empty = std::make_shared<RichPiece>( Piece::empty );
			// Fill the board with empty references
			for(int i=0; i<8; i++) { for(int j=0; j<8; j++) {
				board[i][j] = empty;
			}}
			for(int i=0; i<32; i++) {
				// Copy the PieceList
				piecelist[i] = std::make_shared<RichPiece>(*toCopy.piecelist[i]);
				// Update the pointers inside the SimpleBoard
				if(!piecelist[i]->isEmpty()) {
					board[piecelist[i]->pos.y][piecelist[i]->pos.x] = piecelist[i];
				}
			}
			// As bitboards are POD, do not copy them specially.
			bitb = toCopy.bitb;
			tomove = toCopy.tomove;
			castlerights = toCopy.castlerights;
			enpassant = toCopy.enpassant;
			reversableMoveCount = toCopy.reversableMoveCount;
		}
		Board& operator=(Board toCopy) {
			empty = std::make_shared<RichPiece>( Piece::empty );
			// Fill the board with empty references
			for(int i=0; i<8; i++) { for(int j=0; j<8; j++) {
				board[i][j] = empty;
			}}
			for(int i=0; i<32; i++) {
				// Copy the PieceList
				piecelist[i] = std::make_shared<RichPiece>(*toCopy.piecelist[i]);
				// Update the pointers inside the SimpleBoard
				if(!piecelist[i]->isEmpty()) {
					board[piecelist[i]->pos.y][piecelist[i]->pos.x] = piecelist[i];
				}
			}
			// As bitboards are POD, do not copy them specially.
			bitb = toCopy.bitb;
			tomove = toCopy.tomove;
			castlerights = toCopy.castlerights;
			enpassant = toCopy.enpassant;
			reversableMoveCount = toCopy.reversableMoveCount;
			return *this;
		}
		void move(Move m);   // Make a move to the sub-boards
		int getBitbCoord(Coord c); //Calc bitboard shift amount

		BitboardState bitb;    // Bitboards
		SimpleBoard board;     // Simple 8x8 board array - board[y][x]
		PieceBoard piecelist;  // Array of pieces
		std::shared_ptr<RichPiece> empty;

		// Other board state data
		Piece::Color tomove;
		std::bitset<4> castlerights;
		//blkLng, blkSrt, whtLng, whtSrt
		Coord enpassant;
		int reversableMoveCount;
};

#endif // _BOARD_H_INC
