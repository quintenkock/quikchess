#include "Board.h"

// Create a board with the standard chess setup.
Board::Board() {
	// Make PieceBoard
	empty = std::make_shared<RichPiece>( Piece::empty );
	for(uint_fast8_t i = 0; i<32; i++) {
		piecelist[i] = std::make_shared<RichPiece>();
	}
	// Repeat twice, once for white and once for black.
	for(uint_fast8_t col = 0; col <= 1; col++) { 
		uint_fast8_t row = col * 7;
		uint_fast8_t pawnrow = col?6:1;
		piecelist[col*16 + 0]->recreatePiece(Piece::king,   col);
		piecelist[col*16 + 0]->pos = { row, 4 };
		piecelist[col*16 + 1]->recreatePiece(Piece::queen,  col);
		piecelist[col*16 + 1]->pos = { row, 3 };
		piecelist[col*16 + 2]->recreatePiece(Piece::rook,   col);
		piecelist[col*16 + 2]->pos = { row, 0 };
		piecelist[col*16 + 3]->recreatePiece(Piece::rook,   col);
		piecelist[col*16 + 3]->pos = { row, 7 };
		piecelist[col*16 + 4]->recreatePiece(Piece::bishop, col);
		piecelist[col*16 + 4]->pos = { row, 2 };
		piecelist[col*16 + 5]->recreatePiece(Piece::bishop, col);
		piecelist[col*16 + 5]->pos = { row, 5 };
		piecelist[col*16 + 6]->recreatePiece(Piece::knight, col);
		piecelist[col*16 + 6]->pos = { row, 1 };
		piecelist[col*16 + 7]->recreatePiece(Piece::knight, col);
		piecelist[col*16 + 7]->pos = { row, 6 };
		for(int i=8; i<16; i++) { //8-15 and 24-31 are pawns
			piecelist[col*16 + i]->recreatePiece(Piece::pawn, col);
			uint_fast8_t column = i-8;
			piecelist[col*16 + i]->pos = { pawnrow, column };
		}
	}
	// Make SimpleBoard
	// Note: [2] refers to row 3, because [0] is row 1.
	// Initialize empty
	for(uint_fast8_t i=0; i<8; i++) {
		for(uint_fast8_t j=0; j<8; j++) {
			board[i][j] = empty;
		}
	}
	// Derive board from piecelist
	for(uint_fast8_t i=0; i<32; i++) {
		Coord c = piecelist[i]->pos;
		board[c.y][c.x] = piecelist[i]; //which is an std::shared_ptr
	}

	// Make BitboardState

	// Initialize empty pieces
	bitb[Piece::empty] = 0xffffffffULL << 2*8; //4 rows shifted up two rows.
	// by traversing through the SimpleBoard for the regular pieces
	// and plain making the pawns
	bitb[Piece::whitePawn] = 0xffULL << 1*8; // 6*8 bits 0, 1*8-bits 1, 1*8-bits zero.
	bitb[Piece::blackPawn] = 0xffULL << 6*8; // 1*8 bits 0, 1*8-bits 1, 6*8-bits zero.
	// Note: 0xffULL is 0xff unsigned long long
	
	// Fill the bitboard with the non-pawns
	for(uint_fast8_t i=0; i<32; i++) {
		if(piecelist[i]->getType() == Piece::pawn || piecelist[i]->isEmpty())
			continue;
		bitb[ *piecelist[i] ] |= 0x1ULL << getBitbCoord(piecelist[i]->pos);
	}

	// Finally, set up the Piece::whiteAll and Piece::blackAll
	bitb[Piece::whiteAll] = 0xffffULL << 0;
	bitb[Piece::blackAll] = 0xffffULL << 6*8; // 2 rows yes, rest is no.
}

int Board::getBitbCoord(Coord c) {
	return c.y*8 + (7-c.x);   // 0=h1; 63=a8
	/* 63 62 61 60 59 58 57 56
	 * 55 54 53 52 51 50 49 48
	 * 47 46 45 44 43 42 41 40
	 * 39 38 37 36 35 34 33 32
	 * 31 30 29 28 27 26 25 24
	 * 23 22 21 20 19 18 17 16
	 * 15 14 13 12 11 10 09 08
	 * 07 06 05 04 03 02 01 00
	 */
}

void Board::move(Move m) {
	// TODO: Pawn promotion
	// TODO: Repetitions
	std::shared_ptr<RichPiece> mvPiece = board[m.from.y][m.from.x];

	// Pawn promotion
	if(*mvPiece == Piece::whitePawn && m.to.y == 7) {
		mvPiece->recreatePiece( (Piece::EPiece)(2*m.promotion + 0) );
	} else if(*mvPiece == Piece::blackPawn && m.to.y == 0) {
		mvPiece->recreatePiece( (Piece::EPiece)(2*m.promotion + 1) );
	}

	// Castling
	if(mvPiece->getType() == Piece::king && (m.to.x - m.from.x == 2 || m.to.x - m.from.x == -2) ) {
		// King moved two squares, time to castle.
		std::shared_ptr<RichPiece> castleRook;

		// Move the rook
		if(m.to.x - m.from.x > 0) {
			// Right-castle: short castling.
			move( { board[m.from.y][7]->pos, {m.to.y, static_cast<uint_fast8_t>(m.to.x-1)} });  // Move the rook left of the king's new position
		} else {
			// Left-castle: long castling
			move( { board[m.from.y][0]->pos, {m.to.y, static_cast<uint_fast8_t>(m.to.x+1)} });  // Move the rook right of the king's new position
		}

		// Do not move the king: the king will be moved below this if.
		// Do not update castling rights: this will be done below too.
	}

	// Castling rights
	if(mvPiece->getType() == Piece::king){
		// Remove castling rights for this side
		castlerights &= ~(0x3 << (tomove * 2));
	}
	if(mvPiece->getType() == Piece::rook && ((m.from.y == 0 && tomove == Piece::white) || (m.from.y == 7 && tomove == Piece::black))){
	                                     // Do not be fooled by a moved rook on the same file.
		// Remove castling rights for only one type of castle.
		if(m.from.x == 0) {
			// A-file, so long castle
			castlerights &= ~(0x1 << (tomove*2 + 1));
		} else if(m.from.x == 7) {
			// H-file, so short castle
			castlerights &= ~(0x1 << (tomove*2 + 0));
		}
	}
	// Kill the piece on the To square (if any)
	std::shared_ptr<RichPiece> atkPiece = board[m.to.y][m.to.x];
	if(*atkPiece != Piece::empty) { //not moving to an empty square
		bitb[*atkPiece] ^= 0x1 << getBitbCoord(m.to); //Clear the bitboard.
		bitb[atkPiece->getColor()] ^= 0x1 << getBitbCoord(m.to);

		board[m.to.y][m.to.x]->recreatePiece(Piece::empty);
		// In the piecelist, position of death is recorded in the Coord.

		// Reset reversableMoveCount
		reversableMoveCount = 0;
	} else { //moving to an empty square
		bitb[Piece::empty] ^= 0x1 << getBitbCoord(m.to);
		if(mvPiece->getType() != Piece::pawn) { // Reversible move
			reversableMoveCount++;
		} else {reversableMoveCount = 0;}
	}
	// See above for Reversible move count

	// En-passant
	// Is the moving piece capturing enpassant?
	if(mvPiece->getType() == Piece::pawn && enpassant.y == m.from.y && enpassant.x == m.to.x ){
		// Yes, we are capturing enpassant
		std::shared_ptr<RichPiece> atkPiece = board[enpassant.y][enpassant.x];
		bitb[*atkPiece] ^= 0x1 << getBitbCoord(enpassant); //Clear the bitboard.
		bitb[atkPiece->getColor()] ^= 0x1 << getBitbCoord(enpassant); //Clear the generic bitboard
		board[enpassant.y][enpassant.x]->recreatePiece(Piece::empty);
		// NOTE: Clear the attacked square, this is not necessary in a regular attack because putting the attacking piece there does the trick.
		board[enpassant.y][enpassant.x] = empty;
	}
	// Update en-passant square
	enpassant = {0, 0}; //this equals false in the case of enpassant, as a1 cannot be taken with enpassant capturing
	if(mvPiece->getType() == Piece::pawn && abs(m.to.y - m.from.y) == 2) { //if pawn is moved by two
		enpassant = m.to;
	}

	// Actually move the piece
	// Update SimpleBoard: make the To square point to the piece on the From square
	board[m.to.y][m.to.x] = mvPiece;   //set To
	board[m.from.y][m.from.x] = empty; //clear From
	// Update bitboards
	bitb[*mvPiece] ^= 0x1 << getBitbCoord(m.to);    // Clear old position on bitb
	bitb[mvPiece->getColor()] ^= 0x1 << getBitbCoord(m.to); // also on the allColor bitboard
	bitb[*mvPiece] ^= 0x1 << getBitbCoord(m.from);  // Put new position on bitboard
	bitb[mvPiece->getColor()] ^= 0x1 << getBitbCoord(m.from); // also on the allColor bitboard
	bitb[Piece::empty] ^= 0x1 << getBitbCoord(m.from); // Clear the old position
	// Update PieceList: move the RichPiece to To.
	mvPiece->pos = m.to;

	tomove = (Piece::Color)!tomove; //Swap side that is tomove
}
