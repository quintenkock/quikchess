#include "Search.h"
#include "Eval.h"
#include "Movegen.h"

#include <climits>

int Search::search(Board &b, int depth) {
	if(depth == 0) {
		int who2move = !b.tomove?1:-1;
		return who2move * Eval::eval(b);
	}
	int max = INT_MIN;
	std::vector<Move> moves = Movegen::get_moves(b);
	for(auto m : moves) {
		std::unique_ptr<Board> copyboard = std::make_unique<Board>(b);
		copyboard->move(m);
		int score = -search(*copyboard, depth-1);
		if(score > max) {
			max = score;
		}
	}
	return max;
}

Move Search::rootsearch(Board &b, int depth) {
	Move toReturn;
	int max = INT_MIN;
	std::vector<Move> moves = Movegen::get_moves(b);
	for(auto m : moves) {
		std::unique_ptr<Board> copyboard = std::make_unique<Board>(b);
		copyboard->move(m);
		int score = -search(*copyboard, depth-1);
		if(score > max) {
			max = score;
			toReturn = m;
		}
	}
	return toReturn;
}
