#include "Board.h"
namespace Search {
	struct SearchNode {
		Board b;
		int depth;
		int eval;
	};
	int search(Board &b, int depth);
	Move rootsearch(Board &b, int depth);
}
