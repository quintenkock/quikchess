#include <memory>
#include <vector>
#include "Board.h"

namespace BoardUtil {
	void init();

	bool isCheck(Board &b, Piece::Color c);
	bool isUnderAttack(Board &b, RichPiece &p);
	std::array< std::vector<std::shared_ptr<RichPiece>>, 2> reachableBy(Board &b, RichPiece &p);
	std::vector<std::shared_ptr<RichPiece>> underAttackFrom(Board &b, RichPiece &p);
	std::vector<std::shared_ptr<RichPiece>> defendedBy(Board &b, RichPiece &p);
	std::vector<Coord> generate_line(Coord start, int dx, int dy, int maxdepth);//if(maxdepth==0) maxdepth = infinity

};
