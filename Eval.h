#include "Board.h"
namespace Eval {
	int eval(Board &b);

	const std::array<int, 15> piece_values = 
	{ 0,0, 400,-400, 900,-900, 500,-500, 300,-300, 320,-320, 100,-100, 0 };
	//All, King,     Queen,    Rook,     Bishop,   Knight,   Pawn      Empty

	// up to 80 cpawns bonus for knight. -> stronger in middlegame
	// up to 130 cpawns bonus for bishop -> stronger in endgame
	// up to 140 cpawns bonus for rook.
	// up to 340 cpawns bonus for queen.
	// up to 80 cpawns bonus for king. -> maybe special treatment? -> king safety
	const int mobility_value = 10;
}
