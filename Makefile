bin/quikchess: obj/Main.o obj/Board.o obj/BoardUtil.o obj/Movegen.o obj/Uci.o obj/Search.o obj/Eval.o
	$(CXX) $(CXXFLAGS) obj/Main.o obj/Board.o obj/BoardUtil.o obj/Movegen.o obj/Uci.o obj/Search.o obj/Eval.o -o bin/quikchess

obj/Main.o : Main.cpp Uci.h
	$(CXX) -c $(CXXFLAGS) Main.cpp -o obj/Main.o

obj/Board.o : Board.cpp Board.h Datatypes.h
	$(CXX) -c $(CXXFLAGS) Board.cpp -o obj/Board.o

obj/BoardUtil.o : BoardUtil.cpp BoardUtil.h Board.h Datatypes.h
	$(CXX) -c $(CXXFLAGS) BoardUtil.cpp -o obj/BoardUtil.o

obj/Movegen.o : Movegen.cpp Movegen.h BoardUtil.h Board.h Datatypes.h
	$(CXX) -c $(CXXFLAGS) Movegen.cpp -o obj/Movegen.o

obj/Uci.o : Uci.cpp Uci.h
	$(CXX) -c $(CXXFLAGS) Uci.cpp -o obj/Uci.o

obj/Search.o : Search.cpp Search.h Eval.h Movegen.h Board.h BoardUtil.h Datatypes.h
	$(CXX) -c $(CXXFLAGS) Search.cpp -o obj/Search.o

obj/Eval.o : Eval.cpp Eval.h BoardUtil.h Movegen.h Board.h Datatypes.h
	$(CXX) -c $(CXXFLAGS) Eval.cpp -o obj/Eval.o

clean :
	rm obj/* bin/*
