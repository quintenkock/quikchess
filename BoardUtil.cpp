#include "BoardUtil.h"

#include <bitset>
#include <algorithm>

bool BoardUtil::isCheck(Board& b, Piece::Color c) {
	return isUnderAttack(b, *b.piecelist[c * 16]);
	// note: the c is only correct with the current board representation
}

bool BoardUtil::isUnderAttack(Board& b, RichPiece& p) {
	return !underAttackFrom(b,p).empty();
}

std::array< std::vector<std::shared_ptr<RichPiece>>, 2> BoardUtil::reachableBy(Board& b, RichPiece& p) {
	struct attackLine { std::vector<Piece> attackers; std::vector<Coord> line; };
	std::vector<attackLine> lines;

	// Pawns can only take diag ahead (so a pawn diag behind is a threat)
	lines.push_back({{Piece::whitePawn}, generate_line(p.pos,  1,-1, 1)});
	lines.push_back({{Piece::whitePawn}, generate_line(p.pos, -1,-1, 1)});
	lines.push_back({{Piece::blackPawn}, generate_line(p.pos,  1, 1, 1)});
	lines.push_back({{Piece::blackPawn}, generate_line(p.pos, -1, 1, 1)});
	// Enpassant taking
	if(p.pos == b.enpassant) {
		// This piece can be taken enpassant
		if( p.getColor() == Piece::white ) {
			lines.push_back({{Piece::blackPawn}, generate_line(p.pos,  1, 0, 1)});
			lines.push_back({{Piece::blackPawn}, generate_line(p.pos, -1, 0, 1)});
		} else {
			lines.push_back({{Piece::whitePawn}, generate_line(p.pos,  1, 0, 1)});
			lines.push_back({{Piece::whitePawn}, generate_line(p.pos, -1, 0, 1)});
		}
	}
	// Regular pieces are more similiar to each other in this.
	// Knight
	lines.push_back({{Piece::whiteKnight, Piece::blackKnight}, generate_line(p.pos,  1, 2, 1)});
	lines.push_back({{Piece::whiteKnight, Piece::blackKnight}, generate_line(p.pos,  2, 1, 1)});
	lines.push_back({{Piece::whiteKnight, Piece::blackKnight}, generate_line(p.pos, -1, 2, 1)});
	lines.push_back({{Piece::whiteKnight, Piece::blackKnight}, generate_line(p.pos, -2, 1, 1)});
	lines.push_back({{Piece::whiteKnight, Piece::blackKnight}, generate_line(p.pos,  1,-2, 1)});
	lines.push_back({{Piece::whiteKnight, Piece::blackKnight}, generate_line(p.pos,  2,-1, 1)});
	lines.push_back({{Piece::whiteKnight, Piece::blackKnight}, generate_line(p.pos, -1,-2, 1)});
	lines.push_back({{Piece::whiteKnight, Piece::blackKnight}, generate_line(p.pos, -2,-1, 1)});
	// Bishop
	lines.push_back({{Piece::whiteBishop, Piece::blackBishop}, generate_line(p.pos, 1,1, 0)});
	lines.push_back({{Piece::whiteBishop, Piece::blackBishop}, generate_line(p.pos, -1,1, 0)});
	lines.push_back({{Piece::whiteBishop, Piece::blackBishop}, generate_line(p.pos, 1,-1, 0)});
	lines.push_back({{Piece::whiteBishop, Piece::blackBishop}, generate_line(p.pos, -1,-1, 0)});
	// Rook
	lines.push_back({{Piece::whiteRook, Piece::blackRook}, generate_line(p.pos, 1,0, 0)});
	lines.push_back({{Piece::whiteRook, Piece::blackRook}, generate_line(p.pos, 1,0, 0)});
	lines.push_back({{Piece::whiteRook, Piece::blackRook}, generate_line(p.pos, 1,0, 0)});
	lines.push_back({{Piece::whiteRook, Piece::blackRook}, generate_line(p.pos, 1,0, 0)});
	// Queen
	lines.push_back({{Piece::whiteQueen, Piece::blackQueen}, generate_line(p.pos,  0, 1, 0)});
	lines.push_back({{Piece::whiteQueen, Piece::blackQueen}, generate_line(p.pos,  1, 1, 0)});
	lines.push_back({{Piece::whiteQueen, Piece::blackQueen}, generate_line(p.pos,  1, 0, 0)});
	lines.push_back({{Piece::whiteQueen, Piece::blackQueen}, generate_line(p.pos,  1,-1, 0)});
	lines.push_back({{Piece::whiteQueen, Piece::blackQueen}, generate_line(p.pos,  0,-1, 0)});
	lines.push_back({{Piece::whiteQueen, Piece::blackQueen}, generate_line(p.pos, -1,-1, 0)});
	lines.push_back({{Piece::whiteQueen, Piece::blackQueen}, generate_line(p.pos, -1, 0, 0)});
	lines.push_back({{Piece::whiteQueen, Piece::blackQueen}, generate_line(p.pos, -1, 1, 0)});
	// King
	lines.push_back({{Piece::whiteKing, Piece::blackKing}, generate_line(p.pos,  0, 1, 1)});
	lines.push_back({{Piece::whiteKing, Piece::blackKing}, generate_line(p.pos,  1, 1, 1)});
	lines.push_back({{Piece::whiteKing, Piece::blackKing}, generate_line(p.pos,  1, 0, 1)});
	lines.push_back({{Piece::whiteKing, Piece::blackKing}, generate_line(p.pos,  1,-1, 1)});
	lines.push_back({{Piece::whiteKing, Piece::blackKing}, generate_line(p.pos,  0,-1, 1)});
	lines.push_back({{Piece::whiteKing, Piece::blackKing}, generate_line(p.pos, -1,-1, 1)});
	lines.push_back({{Piece::whiteKing, Piece::blackKing}, generate_line(p.pos, -1, 0, 1)});
	lines.push_back({{Piece::whiteKing, Piece::blackKing}, generate_line(p.pos, -1, 1, 1)});

	// TODO: Use iterators instead?
	std::array<std::vector<std::shared_ptr<RichPiece>>, 2> ret;
	for(auto line : lines) {
		for(auto square : line.line) {
			if(std::find(line.attackers.begin(), line.attackers.end(),
						b.board[square.y][square.x]->getPiece()) != line.attackers.end() ) {
				ret[b.board[square.y][square.x]->getColor()].push_back(b.board[square.y][square.x]);
			}
		}
	}

	return ret;
}

std::vector<std::shared_ptr<RichPiece>> BoardUtil::underAttackFrom(Board& b, RichPiece& p) {
	return reachableBy(b,p)[!p.getColor()];
}
std::vector<std::shared_ptr<RichPiece>> BoardUtil::defendedBy(Board& b, RichPiece& p) {
	return reachableBy(b,p)[p.getColor()];
}

std::vector<Coord> BoardUtil::generate_line(Coord start, int dx, int dy, int maxdepth) {
	std::vector<Coord> line;
	int x = start.x;
	int y = start.y;

	for(int i=0; i<maxdepth || maxdepth == 0; i++) {
		x += dx;
		y += dy;

		if(x < 0 || x > 7 ||    // Out of bounds
		   y < 0 || y > 7) {
			break;
		}
		line.push_back({(uint_fast8_t)y,(uint_fast8_t)x});
	}

	return line;
}
