#include "Board.h"

#include <vector>

namespace Movegen {
	std::vector<Move> get_moves(Board &b);
	std::vector<Move> get_piece_moves(Board &b, RichPiece &p);
}
