#include "Movegen.h"
#include "BoardUtil.h"

std::vector<Move> Movegen::get_moves(Board &b) {
	std::vector<Move> result;
	for(int i=0; i<32; i++) {
		auto p = get_piece_moves(b, *b.piecelist[i]);
		result.insert(result.end(), p.begin(), p.end());
	}
	return result;
}

std::vector<Move> Movegen::get_piece_moves(Board &b, RichPiece &p) {
	if(p == Piece::empty ||        // Empty piece (eg taken)
	   p.getColor() != b.tomove)   // Not from side to move
		{ return {}; }             // Unable to move

	std::vector<Move> moves;

	// Castling
	if(p == Piece::whiteKing) {
		if( 0x1 & (b.castlerights >> 1).to_ulong()) {
			// Long castling rights present!
			RichPiece tmpPiece1;
			RichPiece tmpPiece2;
			tmpPiece1.pos = {3,0};
			tmpPiece2.pos = {2,0};
			if(
					b.board[0][3]->isEmpty() && b.board[0][2]->isEmpty() && 
					b.board[0][1]->isEmpty() && 
					!BoardUtil::isUnderAttack(b, tmpPiece1) &&
					!BoardUtil::isUnderAttack(b, tmpPiece2)
			){
				moves.push_back( {{0,4},{0,2}} );
			}
		}
		if( 0x1 & (b.castlerights >> 0).to_ulong()) {
			// Short castling rights present!
			RichPiece tmpPiece1;
			RichPiece tmpPiece2;
			tmpPiece1.pos = {5,0};
			tmpPiece2.pos = {6,0};
			if(
					b.board[0][5]->isEmpty() && b.board[0][6]->isEmpty() && 
					!BoardUtil::isUnderAttack(b, tmpPiece1) &&
					!BoardUtil::isUnderAttack(b, tmpPiece2)
			){
				moves.push_back( {{0,4},{0,6}} );
			}
		}
	} else if(p == Piece::blackKing) {
		if( 0x1 & (b.castlerights >> 3).to_ulong()) {
			// Long castling rights present!
			RichPiece tmpPiece1;
			RichPiece tmpPiece2;
			tmpPiece1.pos = {3,7};
			tmpPiece2.pos = {2,7};
			if(
					b.board[7][3]->isEmpty() && b.board[7][2]->isEmpty() && 
					b.board[7][1]->isEmpty() && 
					!BoardUtil::isUnderAttack(b, tmpPiece1) &&
					!BoardUtil::isUnderAttack(b, tmpPiece2)
			){
				moves.push_back( {{7,4},{7,2}} );
			}
		}
		if( 0x1 & (b.castlerights >> 2).to_ulong()) {
			// Short castling rights present!
			RichPiece tmpPiece1;
			RichPiece tmpPiece2;
			tmpPiece1.pos = {5,7};
			tmpPiece2.pos = {6,7};
			if(
					b.board[7][5]->isEmpty() && b.board[7][6]->isEmpty() && 
					!BoardUtil::isUnderAttack(b, tmpPiece1) &&
					!BoardUtil::isUnderAttack(b, tmpPiece2)
			){
				moves.push_back( {{7,4},{7,6}} );
			}
		}
	}
	
	std::vector< std::vector<Coord> > lines;
	switch(p) {
		using namespace BoardUtil;
		case Piece::whitePawn:
			// Note: pawns need special treatment
			// If both ahead are clear and current position is on the 2-file, move 2
			if(p.pos.y == 1) {
				if(b.board[p.pos.y+1][p.pos.x]->isEmpty() && b.board[p.pos.y+2][p.pos.x]->isEmpty())
					lines.push_back(generate_line(p.pos, 0, 1, 2));
				else if(b.board[p.pos.y+1][p.pos.x]->isEmpty())
					lines.push_back(generate_line(p.pos, 0, 1, 1));
			} else {
				if(p.pos.y == 6 && b.board[7][p.pos.x]->isEmpty()) {
					// Pawn promotion
					Coord from = p.pos;
					Coord to = {7, p.pos.x};
					moves.push_back({from, to, Piece::knight});
					moves.push_back({from, to, Piece::bishop});
					moves.push_back({from, to, Piece::rook});
					moves.push_back({from, to, Piece::queen});
				} else {
					// Ahead needs to be clear (cannot take straight ahead).
					if(b.board[p.pos.y+1][p.pos.x]->isEmpty()) lines.push_back(generate_line(p.pos, 0, 1, 1));
				}
			}
			// Diagonal taking
			if(!b.board[p.pos.y+1][p.pos.x-1]->isEmpty()) lines.push_back(generate_line(p.pos, -1, 1, 1));
			if(!b.board[p.pos.y+1][p.pos.x+1]->isEmpty()) lines.push_back(generate_line(p.pos,  1, 1, 1));
			// Enpassant taking
			if(b.enpassant.y == p.pos.y && b.enpassant.x ==  p.pos.x-1) {
				lines.push_back(generate_line(p.pos, -1, 1, 1));
			}
			if(b.enpassant.y == p.pos.y && b.enpassant.x ==  p.pos.x+1) {
				lines.push_back(generate_line(p.pos,  1, 1, 1));
			}
			break;
		case Piece::blackPawn:
			// Note: pawns need special treatment
			// If both ahead are clear and current position is on the 2-file, move 2
			if(p.pos.y == 6) {
				if(b.board[p.pos.y-1][p.pos.x]->isEmpty() && b.board[p.pos.y-2][p.pos.x]->isEmpty())
					lines.push_back(generate_line(p.pos, 0, -1, 2));
				else if(b.board[p.pos.y-1][p.pos.x]->isEmpty())
					lines.push_back(generate_line(p.pos, 0, -1, 1));
			} else {
				if(p.pos.y == 1 && b.board[0][p.pos.x]->isEmpty()) {
					// Pawn promotion
					Coord from = p.pos;
					Coord to = {0, p.pos.x};
					moves.push_back({from, to, Piece::knight});
					moves.push_back({from, to, Piece::bishop});
					moves.push_back({from, to, Piece::rook});
					moves.push_back({from, to, Piece::queen});
				} else {
					// Ahead needs to be clear (cannot take straight ahead).
					if(b.board[p.pos.y-1][p.pos.x]->isEmpty()) lines.push_back(generate_line(p.pos, 0, -1, 1));
				}
			}
			// Diagonal taking
			if(!b.board[p.pos.y-1][p.pos.x-1]->isEmpty()) lines.push_back(generate_line(p.pos, -1, -1, 1));
			if(!b.board[p.pos.y-1][p.pos.x+1]->isEmpty()) lines.push_back(generate_line(p.pos,  1, -1, 1));
			// Enpassant taking
			if(b.enpassant.y == p.pos.y && b.enpassant.x == p.pos.x-1) {
				lines.push_back(generate_line(p.pos, -1, -1, 1));
			}
			if(b.enpassant.y == p.pos.y && b.enpassant.x == p.pos.x+1) {
				lines.push_back(generate_line(p.pos,  1, -1, 1));
			}
			break;
		case Piece::whiteKnight:
		case Piece::blackKnight:
			lines.push_back(generate_line(p.pos, 1,2, 1));
			lines.push_back(generate_line(p.pos, 2,1, 1));
			lines.push_back(generate_line(p.pos, -1,2, 1));
			lines.push_back(generate_line(p.pos, -2,1, 1));
			lines.push_back(generate_line(p.pos, 1,-2, 1));
			lines.push_back(generate_line(p.pos, 2,-1, 1));
			lines.push_back(generate_line(p.pos, -1,-2, 1));
			lines.push_back(generate_line(p.pos, -2,-1, 1));
			break;
		case Piece::whiteBishop:
		case Piece::blackBishop:
			lines.push_back(generate_line(p.pos, 1,1, 0));
			lines.push_back(generate_line(p.pos, -1,1, 0));
			lines.push_back(generate_line(p.pos, 1,-1, 0));
			lines.push_back(generate_line(p.pos, -1,-1, 0));
			break;
		case Piece::whiteRook:
		case Piece::blackRook:
			lines.push_back(generate_line(p.pos, 1,0, 0));
			lines.push_back(generate_line(p.pos, 1,0, 0));
			lines.push_back(generate_line(p.pos, 1,0, 0));
			lines.push_back(generate_line(p.pos, 1,0, 0));
			break;
		case Piece::whiteQueen:
		case Piece::blackQueen:
			lines.push_back(generate_line(p.pos,  0, 1, 0));
			lines.push_back(generate_line(p.pos,  1, 1, 0));
			lines.push_back(generate_line(p.pos,  1, 0, 0));
			lines.push_back(generate_line(p.pos,  1,-1, 0));
			lines.push_back(generate_line(p.pos,  0,-1, 0));
			lines.push_back(generate_line(p.pos, -1,-1, 0));
			lines.push_back(generate_line(p.pos, -1, 0, 0));
			lines.push_back(generate_line(p.pos, -1, 1, 0));
			break;
		case Piece::whiteKing:
		case Piece::blackKing:
			lines.push_back(generate_line(p.pos,  0, 1, 1));
			lines.push_back(generate_line(p.pos,  1, 1, 1));
			lines.push_back(generate_line(p.pos,  1, 0, 1));
			lines.push_back(generate_line(p.pos,  1,-1, 1));
			lines.push_back(generate_line(p.pos,  0,-1, 1));
			lines.push_back(generate_line(p.pos, -1,-1, 1));
			lines.push_back(generate_line(p.pos, -1, 0, 1));
			lines.push_back(generate_line(p.pos, -1, 1, 1));
			break;
	}
	// Now, it is time to make this into a list of moves.
	for(std::vector<Coord> line : lines) {
		for(Coord move : line) {
			if( !b.board[move.y][move.x]->isEmpty() ) {
				// When the To is not empty, you cannot move further
				if( b.board[move.y][move.x]->getColor() != p.getColor() ) {
					// I can take the piece on To if the move doesn't leave me in check.
					std::shared_ptr<Board> afterMove = std::make_shared<Board>(b);	//Copy board
					afterMove->move( {p.pos, move} );
					if( !BoardUtil::isCheck(*afterMove, b.tomove) ) {
						moves.push_back( {p.pos, move} );
					}
				}
				break; //Abort this line.
			}
			std::shared_ptr<Board> afterMove = std::make_shared<Board>(b);
			afterMove->move( {p.pos, move} );
			if( !BoardUtil::isCheck(*afterMove, b.tomove) ) {
				moves.push_back( {p.pos, move} );
			}
		}
	}
	return moves;
}
