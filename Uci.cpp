// http://wbec-ridderkerk.nl/html/UCIProtocol.html
#include "Uci.h"
#include "Board.h"
#include "Search.h"

#include <iostream>
#include <limits>
#include <string>

// Main entry point for UCI-mode; only received UCI command yet is 'uci'
int UciMain()
{
	// Identify myself
	std::cout << "id name QuiKchess\n";
	std::cout << "id author Kishan & Quinten Kock\n";

	// Options go here
	
	// Sent all info
	std::cout << "uciok\n";

	// Wait for isready
	std::string cmd;
	while( cmd != "isready")
	{
		std::cin >> cmd;
		// Set options
		if( cmd == "setoption" )
		{
			// UCI wants to set an option;
			// as options are not implemented ignore until eol.
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		if( cmd == "quit" )
		{
			// Stop the engines
			return 0; //No error occured, quit as usual.
		}
	}

	// Init procedures go here
	Board b;
	
	// Init done
	std::cout << "readyok\n";

	// Time to enter the main loop!
	while( cmd != "quit" )
	{
		std::cin >> cmd;
		if( cmd == "ucinewgame" )
		{
			b = Board();
		}
		if( cmd == "position" )
		{
			// TODO: initialize the board with this position.
		}
		if( cmd == "go" )
		{
			Move m = Search::rootsearch(b, 2);
			printf("%i,%i %i,%i\n", m.from.x, m.from.y, m.to.x, m.to.y);
		}
	}

	// Asked to quit; stop the engines
	return 0;
}
