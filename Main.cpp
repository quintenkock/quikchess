#include "Uci.h"

#include <iostream>
#include <string>

int main(int argc, char** argv)
{
	std::string firstcommand;
	std::cin >> firstcommand;
	if(firstcommand == "uci")
	{
		// We are running as an UCI engine
		UciMain();
	}
	if(firstcommand == "xboard")
	{
		// No Xboard support yet: error!
		std::cout << "Xboard not supported!";
		return 1;
	}
}
