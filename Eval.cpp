#include "Eval.h"

#include "BoardUtil.h"
#include "Movegen.h"

#include <climits>


// Evaluate board position: + means good for white, - means good for black.
// Unit is centipawns
int Eval::eval(Board &b) {
	auto moves = Movegen::get_moves(b);
	if(moves.empty()) {
		if( BoardUtil::isCheck(b, b.tomove) ) {
			// Checkmate: return won.
			if(b.tomove == Piece::white)
				return INT_MAX;
			else // Piece::black
				return INT_MIN;
		} else {
			// Stalemate: return draw
			return 0;
		}
	}

	int material = 0;
	// Iterate over the pieces.
	for(auto p : b.piecelist) {
		material += piece_values[*p];
		switch(*p) {
			case Piece::whitePawn:
				// Reward white pawns for going to the 8-file so they can promote.
				material += 5 * p->pos.y^2;
				break;
			case Piece::blackPawn:
				// Reward black pawns for going to the 1-file so they can promote.
				material += 5 * p->pos.y^2;
				break;
			default:
				// Calculate mobility for other pieces
				int mobilMul = !p->getColor()?1:-1;
				material += mobilMul * Movegen::get_piece_moves(b, *p).size();
				break;
		}
	}

	return material;
}
