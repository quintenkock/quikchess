#ifndef _DATATYPES_H_INC
#define _DATATYPES_H_INC

#include <cstdint>

struct Coord {
	uint_fast8_t y;    //1-8
	uint_fast8_t x;    //a-h
};

inline bool operator==(const Coord& a, const Coord& b) {
	return a.y == b.y && a.x == b.x;
}

class Piece {
	public:
		enum EPiece : uint_fast8_t {
			whiteKing   = 2 ,
			whiteQueen  = 4 ,
			whiteRook   = 6 ,
			whiteBishop = 8 ,
			whiteKnight = 10,
			whitePawn   = 12,
			blackKing   = 3 ,
			blackQueen  = 5 ,
			blackRook   = 7 ,
			blackBishop = 9 ,
			blackKnight = 11,
			blackPawn   = 13,
			// Below are for the added bitboards
			whiteAll    = 0,
			blackAll    = 1,
			empty       = 14,
		};
		enum Color : bool {
			white = 0,
			black = 1
		};
		enum Type : uint_fast8_t {
			king =   1,
			queen =  2,
			rook =   3,
			bishop = 4,
			knight = 5,
			pawn =   6,
			// Below aren't actual types but for the added bitboards
			all =    0,
			typeEmpty =  7
		};

		// Functions: only here because enums need to be defined first.
		Piece()           // Empty constr; do recreatePiece later on.
			{ recreatePiece(EPiece::empty); }
		Piece( EPiece pp)
			{ recreatePiece(pp); }
		Piece( Type t, Color c)
			{ recreatePiece(t, c); }
		Piece( Type t, bool b)
			{ recreatePiece(t, b); }
		Color getColor()
			{ return static_cast<Color>(p%2);}
		Type getType()
			{ return static_cast<Type>(p/2);}
		void recreatePiece(EPiece pp)
			{ p = pp; }
		void recreatePiece(Type t, Color c)
			{ p = static_cast<EPiece>(t*2 +c); }
		void recreatePiece(Type t, bool c)
			{ p = static_cast<EPiece>(t*2 +c); }
		bool isEmpty()
			{ return p == EPiece::empty; }
			// omitting EPiece:: is possible, but I prefer using namespaces even inside the function
			// But when using a Piece please use Piece::empty instead of Piece::EPiece::empty

		EPiece getPiece()
			{ return p; }
		operator uint_fast8_t() //Convert to int
			{ return p; }
	private:
		EPiece p;
};

// Piece, but with added data about position
class RichPiece : public Piece{
	public:
		using Piece::Piece;
		Coord pos;
};

//Move without metadata
struct Move {
	Coord from;
	Coord to;
	Piece::Type promotion; // Store the piece to promote to in here, if applicable.
};

#endif //_DATATYPES_H_INC
